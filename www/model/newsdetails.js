// Initial NewsAPI
const news = new News();

// Initial UI
const ui = new UI();

// console.log(window.location.search.split('=')[1]);
const postID = window.location.search.split('=')[1];

news.getSingleNews(postID).then(data => {
  ui.showSingleNews(data);
});
