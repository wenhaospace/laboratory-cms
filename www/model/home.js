// Initial BannersAPI
const banners = new HomeBanners();

// Initial UI
const ui = new HomeBannerUI();

banners.getBanners().then(data => {
  ui.showBanners(data);
});
