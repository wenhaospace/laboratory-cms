// Initial NewsAPI
const news = new News();

// Initial UI
const ui = new UI();

news.getNews().then(data => {
  ui.showNews(data);
});
