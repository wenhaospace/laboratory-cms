class News {
  constructor() {}

  async getNews() {
    const newsResponse = await fetch(`http://localhost:1337/news-posts`);
    // const newsResponse = await fetch(`http://207.148.67.127:1337/news-posts`);
    const news = await newsResponse.json();

    return news;
  }

  async getSingleNews(postID) {
    const newsResponse = await fetch(
      `http://localhost:1337/news-posts/${postID}`
    );
    // const newsResponse = await fetch(
    //   `http://207.148.67.127:1337/news-posts/${postID}`
    // );
    const news = await newsResponse.json();

    return news;
  }
}
