class HomeBanners {
  constructor() {}

  async getBanners() {
    const bannersResponse = await fetch(`http://localhost:1337/home-banners`);
    // const bannersResponse = await fetch(
    //   `http://207.148.67.127:1337/home-banners`
    // );
    const banners = await bannersResponse.json();

    return banners;
  }
}
