// section-1
const ideas = document.querySelectorAll('.idea-wraper .item');
const ideasArr = Array.from(ideas);
function clearAllIdeasHover() {
  ideasArr.forEach(function(item) {
    if (item.classList[3]) {
      item.classList.remove(item.classList[3]);
    }
  });
}
ideasArr.forEach(function(item) {
  item.addEventListener('mouseenter', function(e) {
    clearAllIdeasHover();
    if (e.target.classList.contains('item')) {
      e.target.classList.add('item-active');
    }
  });
});
