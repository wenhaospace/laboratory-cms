class UI {
  constructor() {
    this.newsContainer = document.querySelector('.news-posts');
  }

  //工具函数 - 计算星期
  getweekday(date) {
    var weekArray = new Array(
      '星期日',
      '星期一',
      '星期二',
      '星期三',
      '星期四',
      '星期五',
      '星期六'
    );
    var week = weekArray[new Date(date).getDay()];
    return week;
  }

  showNews(newsList) {
    // console.log(newsList);
    this.newsContainer.innerHTML = ``;
    newsList.forEach((elem, index) => {
      this.newsContainer.innerHTML += `
      <a target="_blank" href="./news-details.html?news-id=${newsList[index].id}" class="item flex-col ">
      <div class="flex-row image" style="background: url(http://localhost:1337${newsList[index].news_cover[0].formats.small.url});"></div>
      <div class="flex-col item-bottom">
        <div class="flex-row date">${newsList[index].news_date}</div>
        <div class="title">
        ${newsList[index].news_title}
        </div>
        <div class="link">阅读详情</div>
      </div>
    </a>
      `;
    });
  }

  showSingleNews(news) {
    // console.log(news);

    // let news_info = document.querySelector('#banner');
    // news_info.setAttribute(
    //   'style',
    //   `background:url('http://localhost:1337${news.new_img.url}') no-repeat center
    // center/cover; !important`
    // );
    // console.log(news_info.style);
    //   news_info.innerHTML = `
    //   <div class="inner">
    //   <div class=" flex-col  cross-align-center main-align-center">
    //     <h1 class="banner-title">${news.new_title}</h1>
    //     <div class="banner-date">${news.new_date} <span>${this.getweekday(
    //     news.new_date
    //   )}</span></div>
    //     <p class="banner-description">
    //       ${news.new_description}
    //     </p>
    //   </div>
    // </div>
    //   `;

    let news_title = document.querySelector('.news-title');
    let news_date = document.querySelector('.news-date');
    news_title.innerHTML = news.news_title;
    news_date.innerHTML = `发布时间：${news.news_date}`;
    // news_image.innerHTML = `<img
    // src="http://localhost:1337${news.news_cover[0].url}"
    // alt=""/>`;

    let news_text = document.querySelector('.news-text');
    // let news_image = document.querySelector('.news-image');
    const converter = new showdown.Converter();
    let converterMd = converter.makeHtml(news.news_content);
    // console.log(converterMd);
    news_text.innerHTML = `
    <div class="news-image">
     <img
      src="http://localhost:1337${news.news_cover[0].url}"
      alt=""/>
    </div>
    ${converterMd}
    `;
  }
}
