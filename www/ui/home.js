class HomeBannerUI {
  constructor() {
    this.carouselItems = document.querySelectorAll('.carousel-item');
  }

  showBanners(data) {
    // console.log(data);
    this.carouselItems.forEach((item, index) => {
      // console.log(item, index);

      item.style.backgroundImage = `url(http://localhost:1337${data[index].backgroundImg[0].url})`;
      // item.style.backgroundImage = `url(http://207.148.67.127:1337${data[index].backgroundImg[0].url})`;
      // console.log(item.style.backgroundImage);
      item.innerHTML = `
      <div class="inner banner-text">
      <h1 class="title">${data[index].title}</h1>
      <p class="slogan">
      ${data[index].description}
      </p>
      <p class="description">
      ${data[index].content}
      </p>
    </div>
      `;
    });
  }
}
